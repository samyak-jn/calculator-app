# Calculator-App

The repository contains an implementation of a `simple calculator app`. It intends to 
perform simple arithmetic operations such as addition, subtractions, multiplications, and divisions.
The app is designed with the help of Java.

The following are the snaps from the app:

![](../snaps/Snap-1)

There is a bit of hiccup with UI, shall rectify soon.
The UI and Installing of App to the mobile was done via gradle on Android Studio platform.
